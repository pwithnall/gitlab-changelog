gitlab-changelog
================

Installation
---

You will need gobject-introspection and the GIR bindings for GLib and
GnomeDesktop installed, in addition to the Python dependencies which can be
installed using `pip`:
```sh
pip install -r ./requirements.txt
```

Usage
---

Generic usage:
```sh
gitlab-changelog.py -H {GITLAB_URI} -t <TOKEN> GNOME/glib 2.58.2..
```
then put the output into `NEWS` or `ChangeLog` and make the release.

The `-H` and `-t` arguments can be omitted by putting the following in
`~/.config/gitlab-changelog.ini`:
```
[gitlab-changelog]
default-hostname = {GITLAB_URI}
[{GITLAB_URI}]
token = <TOKEN>
```

To generate an authentication token for the script, go to
`{GITLAB_URI}/-/user_settings/personal_access_tokens`
and generate a token with the `read_api` scope.
